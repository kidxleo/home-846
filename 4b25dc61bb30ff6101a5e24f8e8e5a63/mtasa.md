| ID | NAME | VERSION | STATUS |
| ------ | ------ | ------ | ------ |
| 4b25dc61bb30ff6101a5e24f8e8e5a63   | MTA:SA   | 0.846.1   | in dev   |   



**Build Instructions**
**[Windows]**

| Prerequisites | 
| ------ | 
| Visual Studio Code | 
| Android NDK >= r22 | 


1) Download the required NDK and place it in the 'ndk' folder, next to your project. 

    It should turn out like this:

    .../your_project/jni/*
    
    .../ndk/android-ndk-r22/*


2) Сreate the 'jni' folder (do as showed in the first step) and put the files from the 'src' folder in the archive into it.
3) Execute 'build.bat' in '/jni/' folder from terminal. (Steps: Terminal > New terminal > ./build.bat)
