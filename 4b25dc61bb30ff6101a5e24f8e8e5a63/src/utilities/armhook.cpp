/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include <sys/mman.h>

#include "main.h"

#define HOOK_PROC "\x01\xB4\x01\xB4\x01\x48\x01\x90\x01\xBD\x00\xBF\x00\x00\x00\x00"
#define LIB_CRASH_OFFSET(lib, addr, offset, value)	*(uint8_t*)(lib + addr + (offset)) = value

ARMHook::ARMHook(uintptr_t lib_ptr, uintptr_t start, uintptr_t end)
{
    this->Reset();

    if(start == 0)
    {
        lib_start = lib_ptr;
    }
    else
    {
        lib_start = lib_ptr + start;
    }
    
    lib_end = lib_start + end;

    mmap_start = (uintptr_t)mmap(0, PAGE_SIZE, PROT_WRITE | PROT_READ | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	mprotect((void*)(mmap_start & 0xFFFFF000), PAGE_SIZE, PROT_READ | PROT_EXEC | PROT_WRITE);
	mmap_end = (mmap_start + PAGE_SIZE);

    // qds team shit !!!!!!!!!!!!!!!!!! REWRITE TODO !!!!!!!!!!!!!!!!!!!!!!!!
    UnFuck(lib_ptr + (start - 0x5E8));
	for (int i = 0; i < 17; i++) {
		LIB_CRASH_OFFSET(lib_ptr, (start - 0x5E8), i, 0);
	}
}

ARMHook::~ARMHook()
{
    this->Reset();
}

void ARMHook::Reset()
{
    lib_start = 0;
    lib_end = 0;
    mmap_start = 0;
    mmap_end = 0;
}

void UnFuck(uintptr_t ptr, size_t dwSize)
{
	if (dwSize)
	{
		unsigned char* to_page = (unsigned char*)((unsigned int)(ptr) & 0xFFFFF000);
		size_t page_size = 0;

		for (int i = 0, j = 0; i < dwSize; ++i)
		{
			page_size = j * 4096;
			if (&((unsigned char*)(ptr))[i] >= &to_page[page_size])
				++j;
		}

		mprotect(to_page, page_size, PROT_READ | PROT_WRITE | PROT_EXEC);
		return;
	}
}

void NOP(uintptr_t addr, unsigned int count)
{
    UnFuck(addr);

    for(uintptr_t ptr = addr; ptr != (addr+(count*2)); ptr += 2)
    {
        *(char*)ptr = 0x00;
        *(char*)(ptr+1) = 0x46;
    }

    cacheflush(addr, (uintptr_t)(addr + count*2), 0);
}

void WriteMemory(uintptr_t dest, uintptr_t src, size_t size)
{
	UnFuck(dest);
	memcpy((void*)dest, (void*)src, size);
	cacheflush(dest, dest+size, 0);
}

void ReadMemory(uintptr_t dest, uintptr_t src, size_t size)
{
    UnFuck(src);
    memcpy((void*)dest, (void*)src, size);
}

void JMPCode(uintptr_t func, uintptr_t addr)
{
	uint32_t code = ((addr-func-4) >> 12) & 0x7FF | 0xF000 | ((((addr-func-4) >> 1) & 0x7FF | 0xB800) << 16);
    WriteMemory(func, (uintptr_t)&code, 4);
}

void WriteHookProc(uintptr_t addr, uintptr_t func)
{
    char code[16];
    memcpy(code, HOOK_PROC, 16);
    *(uint32_t*)&code[12] = (func | 1);
    WriteMemory(addr, (uintptr_t)code, 16);
}

void InstallPLTHook(uintptr_t addr, uintptr_t func, uintptr_t *orig)
{
    UnFuck(addr);
    *orig = *(uintptr_t*)addr;
    *(uintptr_t*)addr = func;
}

void ARMHook::installHook(uintptr_t addr, uintptr_t func, uintptr_t *orig)
{
    if(lib_end < (lib_start + 0x10) || mmap_end < (mmap_start + 0x20)) {
        std::terminate();
    }

    ReadMemory(mmap_start, addr, 4);
    WriteHookProc(mmap_start+4, addr+4);
    *orig = mmap_start+1;
    mmap_start += 32;

    JMPCode(addr, lib_start);
    WriteHookProc(lib_start, func);
    lib_start += 16;
}