#ifndef ARMHOOK_H
#define ARMHOOK_H

class ARMHook
{
private:
    uintptr_t lib_start;
    uintptr_t lib_end;
    uintptr_t mmap_start;
    uintptr_t mmap_end;
public:
    ARMHook(uintptr_t lib_ptr, uintptr_t start, uintptr_t end);
    ~ARMHook();

    void Reset();
    void installHook(uintptr_t addr, uintptr_t func, uintptr_t *orig);
};

void UnFuck(uintptr_t ptr, size_t dwSize = 100);
void NOP(uintptr_t addr, unsigned int count);
void WriteMemory(uintptr_t dest, uintptr_t src, size_t size);
void ReadMemory(uintptr_t dest, uintptr_t src, size_t size);
void JMPCode(uintptr_t func, uintptr_t addr);
void WriteHookProc(uintptr_t addr, uintptr_t func);
void InstallPLTHook(uintptr_t addr, uintptr_t func, uintptr_t *orig);

#endif // ARMHOOK_H