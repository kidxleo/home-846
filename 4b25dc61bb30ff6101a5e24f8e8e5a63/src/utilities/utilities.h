#ifndef UTILITIES_H
#define UTILITIES_H

// types
typedef unsigned long DWORD;
typedef unsigned char BYTE;
typedef unsigned long dword;
typedef unsigned char byte;

class utilities
{
private:
    static char* g_szStorage;
public:
    static uint32_t getTickCount();
    static uintptr_t getLibraryHandle(const char*);
    static void setClientStorage(char*);
    static char* getClientStorage();
};

#endif // UTILITIES_H
