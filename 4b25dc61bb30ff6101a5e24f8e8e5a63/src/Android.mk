LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := hmta
LOCAL_LDLIBS := -llog -lOpenSLES

# main
FILE_LIST += $(wildcard $(LOCAL_PATH)/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/*/*.cpp)

# vendor
FILE_LIST += $(wildcard $(LOCAL_PATH)/vendor/*/*.c)
FILE_LIST += $(wildcard $(LOCAL_PATH)/vendor/*/*.cpp)

LOCAL_SRC_FILES := $(FILE_LIST:$(LOCAL_PATH)/%=%)
LOCAL_CFLAGS := -w
LOCAL_CPPFLAGS := -w -s -fvisibility=default -pthread -Wall -fpack-struct=1 -O2 -std=c++20 -fexceptions

include $(BUILD_SHARED_LIBRARY)