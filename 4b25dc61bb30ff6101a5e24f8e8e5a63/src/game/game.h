#ifndef GAME_H
#define GAME_H

enum eGameVersion
{
    VERSION_ALL = 0,
    VERSION_A_108 = 108,
    VERSION_A_200 = 200,
    VERSION_UNKNOWN = 0xFF
};

class game
{
private:
    /* data */
public:
    game(/* args */);
    ~game() {};

    eGameVersion FindGameVersion() {   
        return *(uint32_t*)(g_GTASAHandle + 0x1AE008) == 0x3F246 ? VERSION_A_108 : VERSION_A_200;
    };
};


#endif // GAME_H