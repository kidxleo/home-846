/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "multiplayer.h"
#include "offsets.h"

void offsets::initialize_1_08()
{
    LOG("initializing offsets..");

    multiplayer::HOOKPOS_CHud_Draw_Caller = g_GTASAHandle + 0x3D6E6C;
}

void offsets::initialize_2_00()
{
    LOG("initializing offsets..");
    // 2.00 TODO ~
}