/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "game/game.h"
#include "offsets.h"
#include "multiplayer.h"

extern ARMHook *pGTASAHook;
extern game *pGameInterface;

bool                multiplayer::m_bSuspensionEnabled;
std::vector<char>   multiplayer::m_PlayerImgCache;
bool                multiplayer::m_bEnabledLODSystem;
bool                multiplayer::m_bEnabledAltWaterOrder;
bool                multiplayer::m_bEnabledClothesMemFix;
float               multiplayer::m_fAircraftMaxHeight;
float               multiplayer::m_fAircraftMaxVelocity;
float               multiplayer::m_fAircraftMaxVelocity_Sq;
bool                multiplayer::m_bHeatHazeEnabled;
bool                multiplayer::m_bHeatHazeCustomized;
float               multiplayer::m_fNearClipDistance;

uintptr_t           multiplayer::HOOKPOS_CHud_Draw_Caller;

void (*CHud_Draw_Caller)(uintptr_t thiz);
void HOOK_CHud_Draw_Caller(uintptr_t thiz);

multiplayer::multiplayer()
{
    LOG("initializing multiplayer..");

    switch(pGameInterface->FindGameVersion()) 
    {
        default:
        case VERSION_A_108: {
            LOG("Game version is 1.08");
            offsets::initialize_1_08();
            break;
        }

        case VERSION_A_200: {
            LOG("Game version is 2.00");
            offsets::initialize_2_00();
            break;
        }
    }

    multiplayer::m_bSuspensionEnabled = true;
    multiplayer::m_fAircraftMaxHeight = 800.0f;
    multiplayer::m_fAircraftMaxVelocity = 1.5f;
    multiplayer::m_fAircraftMaxVelocity_Sq = m_fAircraftMaxVelocity * m_fAircraftMaxVelocity;
    multiplayer::m_bHeatHazeEnabled = true;
    multiplayer::m_bHeatHazeCustomized = false;
}

void multiplayer::InitHooksAndPatches()
{
    LOG("initializing multiplayer hooks and patches..");

    multiplayer::m_fNearClipDistance = DEFAULT_NEAR_CLIP_DISTANCE;

    pGTASAHook->installHook(multiplayer::HOOKPOS_CHud_Draw_Caller, (uintptr_t)HOOK_CHud_Draw_Caller, (uintptr_t*)&CHud_Draw_Caller);
}

void HOOK_CHud_Draw_Caller(uintptr_t thiz)
{
    CHud_Draw_Caller(thiz);

    static bool bFirstCalled = false;

    if(!bFirstCalled) 
    {
        LOG("MultiPlayer started!");
        bFirstCalled = true;
    }
}