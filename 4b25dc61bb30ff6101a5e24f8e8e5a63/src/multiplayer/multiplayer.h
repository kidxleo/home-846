#ifndef MULTIPLAYER_H
#define MULTIPLAYER_H

#include <vector>

#define DEFAULT_NEAR_CLIP_DISTANCE  ( 0.3f )

enum eRadioStationID
{
    UNKNOWN = 0,
    Playback_FM,
    K_Rose,
    K_DST,
    BOUNCE_FM,
    SF_UR,
    RLS,
    RADIO_X,
    CSR_1039,
    K_JAH_WEST,
    Master_Sounds,
    WCTR,
};

class multiplayer
{
    friend class offsets;

private:
    static bool                m_bSuspensionEnabled;
    static std::vector<char>   m_PlayerImgCache;
    static bool                m_bEnabledLODSystem;
    static bool                m_bEnabledAltWaterOrder;
    static bool                m_bEnabledClothesMemFix;
    static float               m_fAircraftMaxHeight;
    static float               m_fAircraftMaxVelocity;
    static float               m_fAircraftMaxVelocity_Sq;
    static bool                m_bHeatHazeEnabled;
    static bool                m_bHeatHazeCustomized;
    static float               m_fNearClipDistance;
    
private:
    static uintptr_t                  HOOKPOS_CHud_Draw_Caller;

public:
    multiplayer();
    ~multiplayer() {};
    void InitHooksAndPatches();
};

#endif // MULTIPLAYER_H