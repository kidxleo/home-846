/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include <jni.h>
#include <android/log.h>
#include <ucontext.h>
#include <pthread.h>

#include "main.h"
#include "core/core.h"
#include "multiplayer/multiplayer.h"

uintptr_t g_GTASAHandle = 0;
uintptr_t g_ClientHandle = 0;
uintptr_t g_SCAndHandle = 0;
uintptr_t g_ImmEmulatorHandle = 0;

ARMHook *pGTASAHook = 0;
multiplayer *pMultiPlayer = 0;

void InitMultiPlayer()
{
	static bool bMultiPlayerInited = false;

	if(!bMultiPlayerInited)
	{
		// initialize core
		core::initialize();

		// initialize multiplayer hooks and patches
		pMultiPlayer = new multiplayer();
		pMultiPlayer->InitHooksAndPatches();

		// StartGameScreen::OnNewGameCheck()
		(( void (*)())(g_GTASAHandle+0x261C8C+1))();

		bMultiPlayerInited = true;
	}
}

unsigned int (*MainMenuScreen__Update)(uintptr_t thiz, float a2);
unsigned int MainMenuScreen__Update_Hook(uintptr_t thiz, float a2)
{
	unsigned int ret = MainMenuScreen__Update(thiz, a2);
	InitMultiPlayer();
	return ret;
}

void InitGlobalHooksAndPatches()
{
	LOG("initializing hooks and patches..");

	// Init hook system for GTASA library
	pGTASAHook = new ARMHook(g_GTASAHandle, 0x180694, 0x1A36);

	/** ============================= HOOKS ============================= */
	pGTASAHook->installHook(g_GTASAHandle + 0x25E660, (uintptr_t)MainMenuScreen__Update_Hook, (uintptr_t*)&MainMenuScreen__Update);

	/** ============================= PATCHES ============================= */
	// cut social club
	UnFuck(g_SCAndHandle + 0x1E16DC);
    strcpy((char*)(g_SCAndHandle + 0x1E16DC), "com/rockstargames/hal/andViewManager");
	UnFuck(g_SCAndHandle + 0x1E1738);
	strcpy((char*)(g_SCAndHandle + 0x1E1738), "staticExitSocialClub");
	UnFuck(g_SCAndHandle + 0x1E080C);
	strcpy((char*)(g_SCAndHandle + 0x1E080C), "()V");
    NOP(g_GTASAHandle + 0x2665EE, 2);

	// FPS hack
	uint8_t fps = 200;
	uint8_t fps_limit = 30;
    WriteMemory(g_GTASAHandle + 0x463FE8, (uintptr_t)&fps, 1);
	WriteMemory(g_GTASAHandle + 0x56C1F6, (uintptr_t)&fps, 1);
	WriteMemory(g_GTASAHandle + 0x56C126, (uintptr_t)&fps, 1);
	WriteMemory(g_GTASAHandle + 0x95B074, (uintptr_t)&fps, 1);
	WriteMemory(g_GTASAHandle + 0x56C1A2, (uintptr_t)&fps_limit, 1);
}

void *mainThread(void *p)
{
	// TODO ~
	pthread_exit(0);
}

jint JNI_OnLoad(JavaVM *vm, void *reserved)
{
	LOG("Library loaded!");
	LOG("Version: " CLIENT_VERSION " Time: " __TIME__ " Date: " __DATE__);

	utilities::setClientStorage("/storage/emulated/0/Android/data/com.rockstargames.gtasa/files/");

	g_GTASAHandle = utilities::getLibraryHandle("libGTASA.so");
	g_ClientHandle = utilities::getLibraryHandle("libhmta.so");
	g_SCAndHandle = utilities::getLibraryHandle("libSCAnd.so");
	g_ImmEmulatorHandle = utilities::getLibraryHandle("libImmEmulatorJ.so");

	srand(time(0));

	crashdump::initialize();

	if(g_ClientHandle && g_GTASAHandle) 
	{
		InitGlobalHooksAndPatches();

		pthread_t thread;
		pthread_create(&thread, 0, mainThread, 0);	
	}

	return JNI_VERSION_1_6;
}

void JNI_OnUnload(JavaVM *vm, void *reserved)
{
	if(pGTASAHook)
	{
		delete pGTASAHook;
		pGTASAHook = 0;
	}

	g_GTASAHandle = 0;
	g_ClientHandle = 0;
	g_SCAndHandle = 0;
	g_ImmEmulatorHandle = 0;
}