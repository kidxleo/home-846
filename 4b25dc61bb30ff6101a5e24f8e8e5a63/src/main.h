#ifndef MAIN_H
#define MAIN_H

#define CLIENT_VERSION "0.846.1"

// API & SDK Includes
#include <cstdlib>
#include <string>
#include <vector>
#include <list>
#include <unistd.h>

extern uintptr_t g_GTASAHandle;
extern uintptr_t g_ClientHandle;
extern uintptr_t g_SCAndHandle;
extern uintptr_t g_ImmEmulatorHandle;

// UTILS
#include "utilities/armhook.h"
#include "utilities/utilities.h"
#include "utilities/log.h"
#include "utilities/crashdump.h"

#endif // MAIN_H